/*Last Modified 2014-12-02 1015 BMN */
$(document).ready(function() {

//----- more stories -----//
    var display = 4;
    var storyLength = $(".storyBlock").length;
    $(".storyBlock").hide();
    $(".storyBlock:lt("+display+")").show();
    
    $("#archives").click(function(){
        if (display < storyLength){
            display = display+4;
            $(".storyBlock:lt("+display+")").show();
        }else{
            $("#archives").hide();
        }
        return false;
    });
    

});// end of js