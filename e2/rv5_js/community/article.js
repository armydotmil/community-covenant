/*global _,document,Backbone,$,window,console,Spinner,news*/
/*jslint white: true*/
/*jslint plusplus: true */

var keyword = 'target_community',
    keyword_url = 'https://www.army.mil/api/packages/getpackagesbykeywords?keywords=' + keyword;

var Article = Backbone.Model.extend({
    urlRoot: keyword_url
});

var Articles = Backbone.Collection.extend({
    model: Article,
    url: keyword_url
});

var Images = Backbone.Collection.extend({
    model: Article,
    url: keyword_url
});

var images = new Images();

var articles = new Articles();

var stories = $('<div>').attr({
    'class': 'stories'
});

var ArticleGallery = Backbone.View.extend({
    initialize: function(options) {
        'use strict';
        var i;
        for (i = 0; i < options.rows; i++) {
            this.listenTo(
                articles,
                'sync',
                _.bind(this.load_articles, this, i)
            );
        }
        this.listenTo(
            images,
            'sync',
            _.bind(this.load_images, this)
        );
        articles.url += '&offset=' + options.offset + '&count=' + options.count;
        articles.fetch();
        images.url += '&offset=' + options.offset + '&count=' + options.count;
        images.fetch();
    },

    load_articles: function(stories) {
        'use strict';
        var shifted = articles.shift();
        if (shifted) {
            buildArticles(shifted);
        }
    },
    load_images: function() {
        'use strict';
        var i;
        for (i = 0; i < images.length; i++) {
            buildImages(images.models[i].get('images'));
        }
		//Slide Show Code
		var prev = $('<i>').attr({'class': 'fa fa-chevron-left fa-4x'})
		var next = $('<i>').attr({'class': 'fa fa-chevron-right fa-4x'})
		$('.featured_photo').append(prev);
		$('.featured_photo').append(next);
		$('.cycle-slideshow').cycle('reinit');
		prepareCaption();
    }
});

function buildImages(images) {
	'use strict';
    var i,
        li,
        img,
        a,
        title,
        desc,
        download,
        download_a,
        caption;

	for (i = 0; i < images.length; i++) {
		
		li = $('<li>').attr({'class': 'slide'});
		var featured_image = $('<div>').attr({'class': 'featured_image'})
		var featured_viewphoto = $('<div>').attr({'class': 'featured_viewphoto'})
		var featured_content = $('<div>').attr({'class': 'featured_content clearfix'})
		img = $('<img>').attr({'alt': images[i].alt, 'width': 298});

    a = $('<a>').attr({'href': images[i].url_size1,'title': images[i].title});

    img.attr('src', images[i].url_size1);
	  
		a.append(img);
		featured_image.append(a);
		featured_content.append(featured_image);
		li.append(featured_content);
            
		var featured_captionTitle = $('<div>').attr({'class': 'featured_captionTitle'});
    	var title = $('<a>').attr({'href': '#'});
		//title.text(images[i].title);
		title.text('Caption');
		featured_captionTitle.append(title);
		
		var featured_captiontext = $('<div>').attr({'class': 'featuredcaption_text'});
		var desc = $('<p>');
		desc.text(images[i].alt);
		featured_captiontext.append(desc);
		
		var featured_imgLink = $('<div>').attr({'class': 'featuredimage_link'});
		var imglink = $('<a>').attr({'href': images[i].url_size1});
		imglink.text('View Image>>');
		featured_imgLink.append(imglink);
//
//         download = $('<div>').attr({
//             'class': 'download'
//         });

//         download_a = $('<a>').attr({
//             'href': images[i].url
//         });
//         download_a.text('Download Original');
//         download.append(download_a);

		var featured_caption = $('<div>').attr({'class': 'featured_caption'});
		featured_caption.append(featured_captionTitle);
		featured_caption.append(featured_captiontext);
		featured_caption.append(featured_imgLink);
//                 caption.append(download);
//
//         img.attr('src', images[i].url_size4);
//         a.append(img);
//         li.append(a);
         li.append(featured_caption);
		$('.featured_photo').append(li);
		
	}
}

function buildArticles(article) {
    'use strict';
    
    var storyBlock = $('<div>').attr({
        'class': 'storyBlock'
    });
    
    var title = article.get('title');
    console.log(title);
    
    var description = article.get('description').substring(0, 200)+' ...';
    console.log(description);
    
    var url = article.get('page_url');
    console.log(url);
    
    var image = article.get('images')[0];
    console.log(image);
    
    //var date = article.get('date');
	var date = sqlToJsDate(article.get('date'));
    console.log(date);
    
    console.log(article);

    var a_image = $('<a>').attr({
        'href': url
    });

    var img = $('<img>').attr({
        'alt': image.alt
        
    });
    
    img.attr('src', image.url_size2);
    
        var h3 = $('<h3>');
        
        var p_date =$('<p>');
        p_date.text(date);
        
        var p_title = $('<a>').attr({
            'href': url
        });
        p_title.text(title);
        
        var p_description = $('<p>');
        p_description.text(description);
        
        var more = $('<a>').attr({
            'href': url
            
        });
        
        more.text(' MORE ');
        
        p_description.append(more);
        
        a_image.append(img);
        
        h3.append(p_title);
//         img = $('<img>'),
//         a, image_a, more, h3;
//
//     img.attr('src', article.get('images')[0].url_size3);
//     a = $('<a>').attr({
//         'href': article.get('page_url')
//     });
//
//     image_a = $('<a>').attr({
//         'href': article.get('page_url')
//     });
//
//     more = $('<a>').attr({
//         'class': 'more',
//         'href': article.get('page_url')
//     });
//
//     more.text(' MORE');
//
//     a.text(article.get('title'));
//     p.text(article.get('description'));
//
//     p.append(more);
//
//     h3 = $('<h3>');
//
//     image_a.append(img);
//     h3.append(a);
//     h3.append(p);
//
storyBlock.append(a_image).append(h3).append(p_date).append(p_description);
stories.append(storyBlock);
    $('#archives').before(stories);

}
//This function prepares the caption to expand and collapse
function prepareCaption () {
	$(".featured_captionTitle").click(function () {
	    $header = $(this);
	    //getting the next element
	    $content = $header.next();
	    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
	    $content.slideToggle(500, function () {
	        //execute this after slideToggle is done
	        //change text of header based on visibility of content div
	        $header.text(function () {
	            //change text based on condition
	            return $content.is(":visible") ? "x Close" : "Caption";
	        });
	    });
	});
}

function sqlToJsDate(sqlDate){
    //sqlDate in SQL DATETIME format ("yyyy-mm-dd hh:mm:ss.ms")
    var sqlDateArr1 = sqlDate.split("-");
    //format of sqlDateArr1[] = ['yyyy','mm','dd hh:mm:ms']
    var sYear = sqlDateArr1[0];
    var sMonth = (Number(sqlDateArr1[1]) - 1).toString();
    var sqlDateArr2 = sqlDateArr1[2].split(" ");
    //format of sqlDateArr2[] = ['dd', 'hh:mm:ss.ms']
    var sDay = sqlDateArr2[0];
    var sqlDateArr3 = sqlDateArr2[1].split(":");
    //format of sqlDateArr3[] = ['hh','mm','ss.ms']
    var sHour = sqlDateArr3[0];
    var sMinute = sqlDateArr3[1];
    var sqlDateArr4 = sqlDateArr3[2].split(".");
    //format of sqlDateArr4[] = ['ss','ms']
    var sSecond = sqlDateArr4[0];
    var sMillisecond = sqlDateArr4[1];

	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";
	var monthName = month[sMonth];
	
	if (sDay.indexOf(0) > -1 & sDay != 10) {
		var newDay = sDay.substring(1,2);
		return monthName + ' ' + newDay + ', ' + sYear;
	}
	else {
		return monthName + ' ' + sDay + ', ' + sYear;
	}
}

// $(window).load(function() {
//     'use strict';
//     initializeSlideShow();
// });


$(document).ready(function() {
    'use strict';

    var articleGallery, options = {
            offset: 1,
            count: 25,
            rows: 4
        }, i;

    if (!articleGallery) {
        articleGallery = new ArticleGallery(options);

        $('#archives').click(function() {
            for (i = 0; i < 4; i++) {
                articleGallery.load_articles();
            }
        });
    }

});